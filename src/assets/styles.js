import HeroImage from "../assets/HeroImage.png";

const windowWidth = window.innerWidth;
const windowHeight = window.innerHeight;
export default {
  navbar: {
          height: "75px",
          position: "fixed",
          backgroundColor:"#3BC84C"
  },
  logo: {width: 80
  },
  container: {
    width: windowWidth * 0.5,
    boxShadow: "0px 1px 4px 1px #676767",
    textAlign: "right",
    backgroundColor: "rgba(59,200,76,.9)",
    paddingTop: 30,
    paddingLeft: 50,
    paddingRight: 50
  },
  background: {
    backgroundImage: `url(${HeroImage})`,
    backgroundSize: "cover",
    height: windowHeight * 1.015
  },
  h1Welcome: {
    color: "#fff",
    fontSize: 70,
    textAlign: "center"
  },
  pWelcome: {
    color: "#fff",
    fontSize: 16,
    textAlign: "left"
  },
  buttonWelcome: {
    marginBottom: 40,
    boxShadow: "0px 1px 4px 1px #b0b0b0",

    borderRadius: "5px",
    backgroundColor: "#fff",
    color: "#3BC84C"
  },
  formContainer: {
    boxShadow: "0px 1px 4px 1px #b0b0b0",
    width: windowWidth * 0.5,
    textAlign: "left",
    backgroundColor: "rgba(255,255,255,.9)"
  },
  formColumn: {
    paddingTop: 30,
    paddingLeft: 50,
    paddingRight: 50
  },
  pForm: { marginTop: 10, marginBottom: 10, fontSize: 18 },
  pFormMargin: { marginBottom: 10, fontSize: 18, marginTop: 40 },
  formError: {
    color: "red",
    marginTop: 10,
    position: "absolute"
  },
  formInput: {
    marginTop: 10
  },

  formInputIcon: {
    color: "#3BC84C"
  },
  formButton: {
    marginBottom: 30,
    boxShadow: "0px 1px 4px 1px #b0b0b0",

    marginTop: 40,
    marginRight: 10,
    borderRadius: "5px",
    backgroundColor: "#3BC84C",
    color: "#fff"
  },
  thanksBox: {
    boxShadow: "0px 1px 4px 1px #b0b0b0",
    width: windowWidth * 0.3,
    backgroundColor: "rgba(255,255,255,.9)",
    padding: 60
  },
  thanksH1: {
    color: "#3BC84C",
    fontSize: 50,
    textAlign: "center"
  },
  errorBox: {
    boxShadow: "0px 1px 4px 1px #b0b0b0",
    width: windowWidth * 0.3,
    backgroundColor: "rgba(255,255,255,.9)",
    padding: 40
  },

  errorH1: {
    color: "#3BC84C",
    fontSize: 40,
    textAlign: "center"
  },
  datetimeInput: {
    readOnly: true,
    placeholder: "01/01/1980"
  },
};
