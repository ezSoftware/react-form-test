import React from "react";
import { Provider } from "react-redux";
import { BrowserRouter as Router, Route } from "react-router-dom";
import "semantic-ui-css/semantic.min.css";
import "react-datetime/css/react-datetime.css";

import AppLayout from "./layouts/app_layout";
import Home from "./pages/home/home";
import store from "./store";

const App = () => {
  return (
    <Provider store={store}>
      <Router>
        <AppLayout>
          <Route exact path="/" component={Home} />
        </AppLayout>
      </Router>
    </Provider>
  );
};

export default App;
