import React from "react";
import { Form, Button, Input, Container, Icon, Grid } from "semantic-ui-react";
import Datetime from "react-datetime";
import { momentToDate } from "../../utils/helper";
import styles from "../../assets/styles";

const formView = ({
  form,
  errors,
  handleFieldChange,
  submit,
  intro,
  handle,
  sent,
  formStatus
}) => {
  return (
    <Grid verticalAlign="middle" centered style={styles.background}>
      <Grid.Row>
        <Grid.Column>
          {!sent ? (
            intro ? (
              <Container style={styles.container}>
                <h1 style={styles.h1Welcome}>Welcome</h1>
                <p style={styles.pWelcome}>
                  Lorem Ipsum is simply dummy text of the printing and
                  typesetting industry. Lorem Ipsum has been the industry's
                  standard dummy text ever since the 1400s, when an unknown
                  printer took a galley of type and scrambled it to make a type
                  specimen book. It has survived not only five centuries, but
                  also the leap into electronic typesetting, remaining
                  essentially unchanged. It was popularised in the 1960s with
                  the release of Letraset sheets containing Lorem Ipsum
                  passages, and more recently with desktop publishing software l
                  ike Aldus PageMaker including versions of Lorem Ipsum.
                </p>
                <p style={styles.pWelcome}>
                  Lorem Ipsum is simply dummy text of the printing and
                  typesetting industry. Lorem Ipsum has been the industry's
                  standard dummy text ever since the 1400s.
                </p>
                <Button
                  size="large"
                  style={styles.buttonWelcome}
                  onClick={() => handle("intro", false)}
                >
                  Next
                </Button>
              </Container>
            ) : (
              <Container style={styles.formContainer}>
                <Form onSubmit={event => event.preventDefault()}>
                  <Grid centered>
                    <Grid.Row columns="2">
                      <Grid.Column width={8} style={styles.formColumn}>
                        <p style={styles.pForm}>First Name*</p>
                        <Input
                          placeholder="Joe"
                          icon
                          fluid
                          style={styles.formInput}
                          onChange={event =>
                            handleFieldChange("firstName", event.target.value)
                          }
                          value={form.firstName}
                        >
                          <input />
                          {form.firstName && (
                            <Icon
                              name="check circle"
                              size="large"
                              style={styles.formInputIcon}
                            />
                          )}
                        </Input>
                        <p style={styles.formError}>
                          {errors.firstNameEmpty &&
                            "First Name can not be blank."}
                        </p>
                        <p style={styles.pFormMargin}>Date of birthday</p>
                        <Datetime
                          style={styles.formInput}
                          inputProps={styles.datetimeInput}
                          onChange={value => handleFieldChange("dob", value)}
                          id="dob"
                          value={momentToDate(form.dob)}
                          readOnly={true}
                          dateFormat="Do/MMM/YYYY"
                          closeOnSelect
                          viewMode="years"
                        />
                        <p style={styles.pFormMargin}>Email Address*</p>
                        <Input
                          placeholder="hello@info.com"
                          icon
                          fluid
                          style={styles.formInput}
                          onChange={event =>
                            handleFieldChange("email", event.target.value)
                          }
                          value={form.email}
                        >
                          <input />
                          {form.email &&
                            !errors.emailError && (
                              <Icon
                                name="check circle"
                                size="large"
                                style={styles.formInputIcon}
                              />
                            )}
                        </Input>
                        <p style={styles.formError}>
                          {errors.emailEmpty && "Email can not be blank."}
                        </p>
                        <p style={styles.formError}>
                          {!errors.emailEmpty &&
                            errors.emailError &&
                            "Please insert a valid email address."}
                        </p>
                      </Grid.Column>
                      <Grid.Column width={8} style={styles.formColumn}>
                        <p style={styles.pForm}>Surname</p>
                        <Input
                          placeholder="Black"
                          icon
                          style={styles.formInput}
                          fluid
                          onChange={event =>
                            handleFieldChange("surname", event.target.value)
                          }
                          value={form.surname}
                        >
                          <input />
                          {form.surname && (
                            <Icon
                              name="check circle"
                              size="large"
                              style={styles.formInputIcon}
                            />
                          )}
                        </Input>
                        <p style={styles.pFormMargin}>Nacionality</p>
                        <Input
                          placeholder="Irish"
                          icon
                          style={styles.formInput}
                          fluid
                          onChange={event =>
                            handleFieldChange("nationality", event.target.value)
                          }
                          value={form.nationality}
                        >
                          <input />
                          {form.nationality && (
                            <Icon
                              name="check circle"
                              size="large"
                              style={styles.formInputIcon}
                            />
                          )}
                        </Input>
                        <p style={styles.pFormMargin}>Occupation</p>
                        <Input
                          placeholder="Front End Developer"
                          icon
                          style={styles.formInput}
                          fluid
                          onChange={event =>
                            handleFieldChange("occupation", event.target.value)
                          }
                          value={form.occupation}
                        >
                          <input />
                          {form.occupation && (
                            <Icon
                              name="check circle"
                              size="large"
                              style={styles.formInputIcon}
                            />
                          )}
                        </Input>
                        <Button
                          size="large"
                          id="button"
                          style={styles.formButton}
                          onClick={() => submit()}
                          floated="right"
                        >
                          Submit
                        </Button>
                      </Grid.Column>
                    </Grid.Row>
                  </Grid>
                </Form>
              </Container>
            )
          ) : formStatus === "ok" ? (
            <Container style={styles.thanksBox}>
              <h1 style={styles.thanksH1}>Thank you</h1>
            </Container>
          ) : (
            <Container style={styles.errorBox}>
              <h1 style={styles.errorH1}>Sorry, something went wrong.</h1>
            </Container>
          )}
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
};

export default formView;
