import React, { Component } from "react";
import { connect } from "react-redux";
import _ from "lodash";
import { submitForm } from "../../actions";
import HomeView from "./home_view";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      form: {
        firstName: "",
        surname: "",
        dob: "",
        nationality: "",
        email: "",
        occupation: ""
      },
      errors: {
        firstNameEmpty: false,
        emailError: false,
        emailEmpty: false
      },
      intro: true,
      sent: false,
      formStatus: "ok" // hardcoding because we have not a real API.
    };
    this.submit = this.submit.bind(this);
    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.handle = this.handle.bind(this);
  }

  /* Commented because we have not a real API .
  componentWillReceiveProps(nextProps) {
    if (!_.isEqual(this.props.formStatus, nextProps.formStatus)) {
      this.setState({ formStatus: nextProps.formStatus });
    }
  }
  */

  handleFieldChange(field, value) {
    this.setState({
      form: Object.assign({}, this.state.form, { [field]: value })
    });
    if (field === "email") {
      if (!/^\w+([-]?\w+)*@\w+([-]?\w+)*(\.\w{2,3})+$/.test(value)) {
        this.setState({
          errors: Object.assign({}, this.state.errors, {
            emailError: true,
            emailEmpty: false
          })
        });
      } else {
        this.setState({
          errors: Object.assign({}, this.state.errors, { emailError: false })
        });
      }
    }
  }

  handle(field, value) {
    this.setState({ [field]: value });
  }

  submit() {
    let errors = {};
    if (_.isEmpty(this.state.form.firstName)) {
      errors.firstNameEmpty = true;
    }
    if (_.isEmpty(this.state.form.email)) {
      errors.emailEmpty = true;
    }
    if (
      !/^\w+([-]?\w+)*@\w+([-]?\w+)*(\.\w{2,3})+$/.test(this.state.form.email)
    ) {
      errors.emailError = true;
    }
    if (_.isEmpty(errors)) {
      this.props.submitForm(this.state.form);
      this.setState({ sent: true });
    }
    this.setState({ errors: errors });
  }

  render() {
    return (
      <HomeView
        form={this.state.form}
        errors={this.state.errors}
        handleFieldChange={this.handleFieldChange}
        submit={this.submit}
        handle={this.handle}
        intro={this.state.intro}
        sent={this.state.sent}
        formStatus={this.state.formStatus}
      />
    );
  }
}

const mapStateToProps = ({ formStatus }) => ({
  formStatus
});

const actions = {
  submitForm
};

export default connect(
  mapStateToProps,
  actions
)(Home);
