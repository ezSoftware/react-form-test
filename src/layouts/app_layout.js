import React, { Component } from "react";
import { Menu, Image } from "semantic-ui-react";
import styles from "../assets/styles";
const logo = require("../assets/mwgLogoWhite.png");

class AppLayout extends Component {
  render() {
    return (
      <div>
        <Menu secondary fixed="top" visible="true" style={styles.navbar}>
          <Menu.Item position="left">
            <Image src={logo} style={styles.logo} />
          </Menu.Item>
        </Menu>
        <div>{this.props.children}</div>
      </div>
    );
  }
}
export default AppLayout;
