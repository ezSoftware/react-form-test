import axios from "axios";
import ROOT_URL from "./root_url";

const axiosConfig = {
  baseURL: ROOT_URL,
  headers: {
    "Content-Type": "application/json"
  }
};

export const axiosInstance = axios.create(axiosConfig);
