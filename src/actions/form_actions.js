import { axiosInstance } from "./configured_axios";
import { SUBMIT_FORM, SUBMIT_FORM_ERROR } from "./action_types";

export function submitForm(form) {
  return dispatch => {
    axiosInstance
      .post(`/api/submit_form`, {
        form
      })
      .then(response => {
        dispatch({ type: SUBMIT_FORM, payload: response });
      })
      .catch(error => dispatch({ type: SUBMIT_FORM_ERROR, payload: error }));
  };
}
