const ROOT_DOMAIN = "api.mwg.com";
const ROOT_URL = `https://${ROOT_DOMAIN}`;

export default ROOT_URL;
