import { SUBMIT_FORM, SUBMIT_FORM_ERROR } from "../actions/action_types";

export function formReducer(state = {}, action) {
  switch (action.type) {
    case SUBMIT_FORM: {
      return "ok";
    }
    case SUBMIT_FORM_ERROR: {
      return "error";
    }
    default: {
      return state;
    }
  }
}
