import { combineReducers } from "redux";
import { formReducer } from "./form_reducer.js";

const rootReducer = combineReducers({
  formStatus: formReducer
});

export default rootReducer;
