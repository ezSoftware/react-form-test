import moment from "moment";

export function momentToDate(momentDate) {
  let newDate = "";
  if (momentDate) {
    newDate = moment(momentDate).format("DD/MMM/YYYY");
  }
  return newDate;
}
